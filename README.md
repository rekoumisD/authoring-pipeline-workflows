# Authoring Pipeline Workflows

This project is a WIP.  The documentation needs more work, as well as comments in files and some of the rules in both the conditional includes in .gitlab-ci.yml and the reference rule list in pipelines/rules.yml.

## Getting started    

If you are a pipelines advisor for your team, or you need to develop a complex workflow that may consist of multiple independent pipelines that run under differing circumstances, setting up projects like this one can allow for rapid development of the workflow independent of creating the logic needed for each job.  The premise is to create a project where you can quickly create and test pipelines consisting of very simple jobs that act as placeholders until you have the workflow you desire.

The key to authoring complex pipelines is to become very familiar with the options you have available when using [rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-when-jobs-run-with-rules) in conjunction with [GitLab's predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).  The jobs contained in the pipelines in this project will dump their variables in the job log to allow for inspection of what is available for any particular scenario, so authors can use simple tools like their browser based searches to find the needed variables.  The script section of each job is one line, `env`, which in Linux will print all variables exposed in the shell/terminal under Linux.  The same command, `env`, will also work in MacOS shell runners.  For Windows users, using powershell, `dir env:` should do the same thing.    

There is a single job just above the file inclusions at the end of .gitlab-ci.yml, 'test-variables',  that is disabled now but can be enabled by changing the `when: never` in the rules to `when: always`.  Doing this will cause that specific job to run unconditionaly for any pipeline trigger condition, which allows you to inspect its variables produced under the trigger you are designing a specific pipeline for.

In the interest of writing and testing quickly, I am not following the standard convention of making jobs hidden and then extending them in downstream job cases.  This eliminates the need to write one more line of code for each job to address extending the hidden job.  GitLab's YML standards allow you to declare jobs a second time (last declaration wins) and simply overwrite any properties of the upstream job you need to.  The jobs are all created in the first inclusion with simple rules that will never let them run.  When I declare them the second time, I write in a rule that will allow them to run that is in accordance with the specific pipeline I am creating in that file.

The pipelines in this project are covered in more detail in [pipelines/README.md](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/ci-cd-examples/authoring-pipeline-workflows/-/tree/main/pipelines).

While you can use docker runners with a simple image to accomplish the needed jobs workflow creation, I prefer to use shell executors (the runner is on my laptop) which can run in seconds and complete the needed variable dump.  This allows for testing and inspecting a variety of differing scenarios without spending large amounts of time conducting the needed tests.  Most of the pipelines defined in this project can complete in about a minute.  The longest pipelines with many stages will complete in about two minutes.

The example pipelines provided here support a varity of scenarios.  This is not necessarily any type of recommendation for patterns you could implement.  They are here to present options for different scenarios that may be similar to what you need to design for you or your team, and to provide implementation details you can review for their rules.

## GitLab Runner

As stated above, I like to use a runner registered from my laptop for developing pipeline workflows.  This allows me to use both shell and docker-machine executors for testing various scenarios.

In Linux workstations (my choice of OS), this is a fairly straightforward process to install and register.  I start by installing GitLab runner following the [instructions in GitLab's documentation](https://docs.gitlab.com/runner/install/).  If you plan to test with Docker containers, you will also need to install [Docker Engine](https://docs.docker.com/engine/install/) before registering your runner.  In Linux, I also had to add the `gitlab-runner` user that GitLab creates to the `docker` group.

Given I don't want my runner always running, I disable the service in my OS so that it does not automatically startup with each boot, then manually start the gitlab-runner service when I want to use it, followed by manually stopping the service when I am done.  If you choose to run a VM with Linux in Windows or MacOS, you can simply start it when ready to test, and stop it when you are done.

The examples below are from my Ubuntu 22.04 laptop:

### Disable the GitLab Runner service after installing it.
`sudo systemctl disable gitlab-runner`

### Start GitLab Runner when I am ready to test.
`sudo service gitlab-runner start`

### Stop GitLab Runner when I am done testing.
`sudo service gitlab-runner stop`


